# Open Oximeter Electronics

{{BOM}}






## Attach nano {pagestep}

* Take the [custom motherboad]{qty:1} and insert the [Arduino Nano]{qty:1} as shown.
* Solder the pins of the nano to the motherboard.
* Trim the excess of the pins.

![](images/step0_placenano.jpg)
![](images/step1_nano.jpg)
![](images/step2_trimpins.jpg)

## Solder jack and header {pagestep}

Solder the [2.5mm power jack]{qty:1} and [2pin 2.54mm SIL Header]{qty:1} to the motherboard.

![](images/step3_solderjack.jpg)
![](images/step4_solderheader.jpg)

## Attach link {pagestep}
Connect the [2.54 link]{qty:1} to the header.

![](images/step5_link.jpg)


## Solder 4-pin socket {pagestep}
Solder the [4pin 2.54mm SIL socket]{qty:1} at the far end of the motherboard

![](images/step6_socket.jpg)

## Solder breakout board  {pagestep}

* Insert the [MAX30100 or MAX30101 Breakout Board] into the mother board on the opposite side to the Nano
* Solder the breakout board connections

![](images/step7_soldermax3010x.jpg)
![](images/step8_max3010x.jpg)

## Connect the display {pagestep}

Using the [4pin 2.54mm SIL Header]{qty:1} connect the [SSD1306 OLED display breakout board]{qty:1} to the 4-pin socket.

![](images/step9_oled_header.jpg)
![](images/step10_final_assembly.jpg)

## Next steps

The electronics are now ready for mechanical assembly of the finger clip or firmware upload. Make sure that firmware is loaded onto the nano before assembly of the finger clip.